
import React from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
} from 'react-native';

import { NavigationContainer } from '@react-navigation/native';
import Splash from './src/screens/Splash';
import Welcome from './src/screens/Welcome';
import Welcome_1 from './src/screens/Welcome_1';
import Welcome_2 from './src/screens/Welcome_2';
import Welcome_3 from './src/screens/Welcome_3';
import Welcome_4 from './src/screens/Welcome_4';

import RISR_SignUp_Step4 from './src/screens/RISR_SignUp_Step4';
import Login_screen from './src/screens/Login_screen';
import Signup_step_2 from './src/screens/Signup_step_2';
import Signup_step_3 from './src/screens/Signup_step_3';
import Signup_step_5 from './src/screens/Signup_step_5';
import RISR_Signup from './src/screens/RISR_Signup';
import Lesson_search from './src/screens/Lesson_search';
import Home_1 from './src/screens/Home_1';
import Home_2 from './src/screens/Home_2';
import Home_3 from './src/screens/Home_3';


import DefaultWelcome from './src/screens/DefaultWelcome';
import { createStackNavigator, TransitionPresets } from '@react-navigation/stack';

const Stack = createStackNavigator();

const App = (props) => {
  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{
          headerShown: false,
          ...TransitionPresets.SlideFromRightIOS
        }}
        mode={"modal"}
        initialRouteName="Splash">
        <Stack.Screen name="Splash" component={Splash} />
        <Stack.Screen name="Welcome_1" component={Welcome_1} />
        <Stack.Screen name="Welcome_2" component={Welcome_2} />
        <Stack.Screen name="Welcome_3" component={Welcome_3} />
        <Stack.Screen name="Welcome_4" component={Welcome_4} />
        <Stack.Screen name="Home_1" component={Home_1} />
        <Stack.Screen name="Home_2" component={Home_2} />
        <Stack.Screen name="Home_3" component={Home_3} />
        <Stack.Screen name="Welcome" component={Welcome} />
        <Stack.Screen name="DefaultWelcome" component={DefaultWelcome} />
        <Stack.Screen name="Login_screen" component={Login_screen} />
        <Stack.Screen name="RISR_Signup" component={RISR_Signup} />
        <Stack.Screen name="Signup_step_2" component={Signup_step_2} />
        <Stack.Screen name="Signup_step_3" component={Signup_step_3} />
        <Stack.Screen name="RISR_SignUp_Step4" component={RISR_SignUp_Step4} />
        <Stack.Screen name="Signup_step_5" component={Signup_step_5} />
        <Stack.Screen name="Lesson_search" component={Lesson_search} />

      </Stack.Navigator>
    </NavigationContainer>
  )
}

export default App;
