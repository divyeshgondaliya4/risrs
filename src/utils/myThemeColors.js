import LinearGradient from 'react-native-linear-gradient';

export default  myThemeColors = {
      
       black:"#000000",
       black_one:"#1E1E1E",
       light_black:"#272727",
       white:"#FFFFFF",
       purple:"#C170F3",
       primary:"92.26deg, #C170F3 4.66%, #64D2FF 93.99%",
       purple_one:"180deg, #755DE9 0%, #5E5CE6 100%",
       gray:"#808080",
       sky_blue:"#64D2FF",
       light_gray:"#C4C4C4",
       lightdove:" #9D9D9D",
       gray_one:"#C6C6C6",
       point:"#D2D2D2",
       purple_one:"#755DE9",
}