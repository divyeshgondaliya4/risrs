import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

export default myFontSize = {

    h1:{
      fontFamily:"RevolutionGothic_Regular",
      fontSize:hp('2.3%'),
     fontWeight:"bold"
    },
    h2:{
      fontFamily:"RevolutionGothic_Regular",
      fontSize:hp('2.3%'),
      fontWidth:'bold',
    },
    h3:{
      fontFamily:"Halyard Text Regular",
      fontSize:hp('2.2%'),
    },
    h4:{
      fontFamily:"Halyard Text Regular",
      fontSize:hp('2.3%'),
    },
    h5:{
      fontFamily:"RevolutionGothic_Regular_It",
      fontSize:hp('4.1%'),
      fontWidth:'bold',
    },
    h6:{
      fontFamily:"RevolutionGothic_Regular",
      fontSize:hp('3%'),
      fontWidth:800,
    },
    h7:{
      fontFamily:"RevolutionGothic_Regular",
      fontSize:hp('2%'),
      fontWidth:800,
    },
    h8:{
      fontFamily:"RevolutionGothic_ExtraBold_It",
      fontSize:hp('1.6%'), 
    },
    h9:{
      fontFamily:"Halyard Text Regular",
      fontSize:hp('1.9%'),
    },
    h10:{
      fontFamily:"RevolutionGothic_ExtraBold",
      fontSize:hp('5.5%'),
    },
    h11:
    {
      fontFamily:"RevolutionGothic_ExtraBold",
      fontSize:hp('1.8%'),
    },
    h12:{
      fontFamily:"RevolutionGothic_ExtraBold",
      fontSize:hp('4.5%'),
    },
    h13:  
    {
      fontFamily:"RevolutionGothic_ExtraBold_It",
      fontSize:hp('3%'), 

    },
    h14:
    
      {
        fontFamily:"RevolutionGothic_ExtraBold_It",
        fontSize:hp('3.3%'), 
  
      },
    
  }