import React, { useState, useCallback, useRef } from 'react';
import {
    View,
    Text,
    Image,
    StyleSheet,
    StatusBar,
    TouchableOpacity,
    TextInput,
    Keyboard,
    Dimensions
} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import myThemeColors from '../utils/myThemeColors';
import myFontSize from '../utils/myFontSize';
import LinearGradient from 'react-native-linear-gradient';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
let { height, width } = Dimensions.get("window")
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

export default function SignUp(props) {
    return (
        <KeyboardAwareScrollView style={styles.container} extraHeight={hp('15%')} showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false}>
        <View style={styles.container} >
            <View style={styles.header_view} >
                <TouchableOpacity onPress={() => { props.navigation.goBack() }} style={styles.lefft_eerow_icon_view}>
                    <Image source={require("../assets/icon/BackButton.png")} resizeMode="contain" style={styles.lefft_errow_icon} />
                </TouchableOpacity>
                <View style={styles.header_text_view} >
                    <Text style={styles.header_text}>REGISTER FOR RISR</Text>
                </View>
            </View>
            <View style={styles.progress_bar_view}>
                <View style={styles.progres_bar_style}></View>
                <View style={styles.progres_bar_style1}></View>
                <View style={styles.progres_bar_style2}></View>
                <View style={styles.progres_bar_style2}></View>
                <View style={styles.progres_bar_style2}></View>
            </View>      
            <View style={styles.profile_view2} >
                <View style={styles.profile_view}></View>
            </View>
            <View style={styles.main_text_view}>
                <Text
                    style={styles.main_text}
                >CHOOSE A</Text>
                <Text
                    style={styles.main_text}
                >PASSWORD</Text>
            </View>
            <View style={styles.text_view}>
                <Text
                    style={styles.text}
                >Lorem ipsum dolor sit amet, consectetur</Text>
                <Text
                    style={styles.text}
                >adipiscing elit, sed do eiusmod tempor.</Text>
            </View>
            <View style={styles.second_contain}>
                <View style={styles.passwordview}>
                    <TextInput style={styles.all_lable_color}
                        placeholder="Password"
                        placeholderTextColor="#C6C6C6"

                    />
                </View>
                <View style={styles.passwordview}>
                    <TextInput style={styles.all_lable_color}
                        placeholder="Confirm Password"
                        placeholderTextColor="#C6C6C6"
                    />
                </View>
                <View style={styles.linear}>
                    <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
                        colors={['#C170F3', '#64D2FF',]}
                        style={styles.linearstyle}
                    >
                        <TouchableOpacity onPress={() => { props.navigation.navigate('Signup_step_3') }} style={styles.linear_button_style}><Text style={styles.next_text}>NEXT</Text></TouchableOpacity>

                    </LinearGradient>
                </View>
            </View>

        </View>
        </KeyboardAwareScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        height: hp('100%'),
        width: wp('100%'),
        backgroundColor: myThemeColors.black,

    },

    header_view:
    {
        width: wp('90%'),
        height: hp('8%'),
        flexDirection: "row",
        alignSelf: "center",
        justifyContent: "center"
    },
    lefft_eerow_icon_view:
    {
        width: wp('19%'),
        height: hp('8%'),
        justifyContent: "center",


    },
    linear:
    {
        width: wp('85%'),
        height: hp('20%'),
        // position:"absolute",
        // bottom:0,
        // marginBottom:hp("10%"),
        alignSelf: "center",
        justifyContent: "center"

    },
    linear_button_style:
    {
        width: wp('85%'),
        height: hp('7%'),
        justifyContent: "center",
        alignItems: "center",

    },
    linearstyle:
    {
        borderRadius: hp("7%") / 2,
    },
    lefft_errow_icon:
    {
        width: wp('6%'),
        height: hp('3%'),
    },
    header_text_view:
    {
        width: wp('71%'),
        height: hp('8%'),
        justifyContent: "center",
},
    next_text:
    {
        color: "#fff",
        fontSize: hp("2.3%"),
        fontFamily: 'RevolutionGothic_ExtraBold',
    },
    header_text: {

        fontSize: hp("3%"),
        color: myThemeColors.white,
        fontFamily: 'RevolutionGothic_ExtraBold',
        marginLeft: hp('3%'),
    },
    progress_bar_view: {
        width: wp('85%'),
        height: hp('10%'),
        // backgroundColor: "#fff"
    },
    passwordview: {
        height: hp('7%'),
        width: wp('85%'),
        borderRadius: hp('4%'),
        alignItems: "center",
        justifyContent: "center",
        color: "#262626",
        backgroundColor: "#262626",
        borderWidth: hp('0.1%'),
        borderColor: "#fff",
        marginTop: hp('3%'),
    },
    second_contain: {
        height: hp('40%'),
        width: wp('100%'),
        alignItems: "center",
        marginTop: hp('1.5%')
    },
    singin_view: {
        height: hp('7%'),
        width: wp('85%'),
        borderRadius: hp('4%'),
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "#262626",
        marginTop: hp('2%')
    },
    sigin_touch: {
        height: hp('7%'),
        width: wp('85%'),
        alignItems: "center",
        justifyContent: "center",
    },
    all_lable_color: {
        color: "#ffffff",
        fontSize: hp('2%'),
        fontFamily: myFontSize.h3.fontFamily,
    },
    progress_bar_view: {
        width: wp('85%'),
        height: hp('7.5%'),
        alignSelf: "center",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center"
        // backgroundColor: "#fff"
    },
    progres_bar_style:
    {
        width: hp("8.5%"),
        height: hp("0.5%"),
        backgroundColor: "#64D2FF",
        borderRadius: hp("0.5%") / 2
    },
    progres_bar_style1:
    {
        width: hp("8.5%"),
        height: hp("0.5%"),
        marginLeft: wp("1%"),
        backgroundColor: "#64D2FF",
        borderRadius: hp("0.5%") / 2
    },
    progres_bar_style2:
    {
        width: hp("8.5%"),
        height: hp("0.5%"),
        marginLeft: wp("1%"),
        backgroundColor: "#3A6F84",
        borderRadius: hp("0.5%") / 2
    },
    profile_view1: {
        height: hp("22%"),
        width: wp("100%"),
        // borderRadius:hp("0%"),
        alignItems: "center",
        marginTop: hp('3.5%'),
        justifyContent: "center"
    },
    profile_view2 : {
        height:hp("24%"),
        width:wp("100%"),
        // borderRadius:hp("0%"),
        alignItems:"center", 
        marginTop:hp('2.5%'), 
        justifyContent:"center"
    },
    profile_view : {
        borderRadius: Math.round(Dimensions.get('window').width + Dimensions.get('window').height) / 2,
        width: Dimensions.get('window').width * 0.45,
        height: Dimensions.get('window').width * 0.45,
        backgroundColor:myThemeColors.gray_one,
        justifyContent: 'center',
        alignItems: 'center'
        
        
    },
    main_text_view: {
        height: hp('15%'),
        width: wp("90%"),
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        // marginTop:hp('2%'),
        // marginVertical:hp('0.5%')
    },
    main_text: {
        color: myThemeColors.white,
        fontFamily: myFontSize.h8.fontFamily,
        fontSize: hp('5.5%'),

    },
    text_view: {
        height: hp('3%'),
        width: wp('80%'),
        alignSelf: 'center',
        alignItems: 'center',
        marginTop: hp('1.3%'),
        justifyContent: 'center',
        // marginVertical:hp('1%'),

    },
    text: {
        color: myThemeColors.white,
        fontFamily: myFontSize.h9.fontFamily,
        fontSize: myFontSize.h9.fontSize
    },
})