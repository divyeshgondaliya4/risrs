import React, { useState, useCallback, useRef } from 'react';
import {
    View,
    Text,
    Image,
    StyleSheet,
    StatusBar,
    TouchableOpacity,
    TextInput,
    Keyboard,
    ImageBackground,
    Dimensions
} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import myThemeColors from '../utils/myThemeColors';
import myFontSize from '../utils/myFontSize';
import LinearGradient from 'react-native-linear-gradient';
import { Colors } from 'react-native/Libraries/NewAppScreen';
import {Drawer,Risr,Ellipse_9,Mask_Group_bg,Fire,Menu_icon} from '../utils/appContant';
import {CardStyleInterpolators, Header} from '@react-navigation/stack';
import {FlatList} from 'react-native-gesture-handler';
let { height, width } = Dimensions.get("window")
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
 
const Data = [
    {
        id: 1,
        image: require("../assets/image/Profile_image_1.png"),
        name: "Esther Howard just finished “Hitting Mentality”",  
    },
    {
        id: 2,
        image: require("../assets/image/Profile_image_2.png"),
        name: "Esther Howard just finished “Hitting Mentality”",
    },
    {
        id: 3,
        image: require("../assets/image/Profile_image_1.png"),
        name: "Esther Howard just finished “Hitting Mentality”",
    },
];
const list = [
    {
        id: 1,
        text: "HITTING LESSONS",
    },
    {
        id: 2,
        text: "MENTAL TRAINNING LESSONS",
    },
    {
        id: 3,
        text: "HITTING LESSONS",
    },
    {
        id: 4,
        text: "MENTAL TRAINNING LESSONS",
    },
    {
        id: 5,
        text: "MENTAL TRAINNING LESSONS",
    },
];

export default function Home_Screen_3 (props) {
    return(
        <View style={styles.container} >
                <LinearGradient start={{ x:0 , y: 1.5 }} end={{ x: 0, y: -2 }}
                    colors={['#1F1F1F', '#000000',]}
                    style={styles.linearstyle1}
                > 
            <View style={styles.header_view}>
                <View style={ styles.Drawer_icon}>
                    <View style={styles.header_img_view}>
                        <TouchableOpacity><Image style={styles.header_img} source={Menu_icon}></Image></TouchableOpacity>
                    </View>
                    <View style={styles.logo_view}>
                       <Image style={styles.logo} source={Risr}></Image>
                    </View>
                    <View style={styles.header_image_view}>
                        <TouchableOpacity><Image style={styles.header_image} source={Ellipse_9}></Image></TouchableOpacity>
                    </View>
                </View>
            </View>
            <View style={styles.image_outer_view}>
                <View style={styles.outer_img_text}>
                    <Text style={styles.on_desk_text}>
                       On Deck 
                    </Text>
                </View>
                <ImageBackground source={Mask_Group_bg} resizeMode="stretch" style={styles.mask_img_view} >
                    <View style={ styles.mask_inner_view}>
                        <Text style={styles.mask_inner_text}>
                            Lesson/Program title
                        </Text>
                    </View>
                </ImageBackground> 
            </View>
            <View style={styles.linear}>
                <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
                    colors={['#C170F3', '#64D2FF',]}
                    style={styles.linearstyle}
                >
                    <TouchableOpacity onPress={()=>{props.navigation.navigate('RISR_Signup')}} style={styles.linear_button_style}>
                        <Text style={styles.next_text}>TAKE A NEW ASSESSMENT</Text>
                    </TouchableOpacity>
                </LinearGradient>
            </View>
            <View style={styles.streak_point_outer_view}> 
                <View style={styles.streak_point_view}>
                    <View style={styles.streak_view}>
                        <Text style={styles.streak_text}>
                            DAILY STREAK  
                        </Text>
                        <View style={styles.streak_number_view}>
                            <Text style={styles.text_two_view}>
                                2
                            </Text>
                            <Image source={Fire} resizeMode="contain" style={styles.fire_img}/> 
                        </View>
                    </View>
                    <View style={styles.point_view}>
                        <Text style={styles.point_first_text}>
                            NEXT TROPHY IN
                        </Text>
                        <View style={styles.hundred_view}>
                        <Text style={styles.point_second_text}>
                            100
                        </Text>
                        </View>
                        <Text style={styles.point_third_text}>
                            POINTS
                        </Text>
                    </View>
                </View>
            </View>
            <View style={styles.first_flatlsit_main}>
                <View style={styles.first_flatlsit_inner}>
                    <View style={styles.flat_first_text}>
                        <Text style={styles.first_text_style}>
                            What Your Friends are Doing
                        </Text>
                    </View>
                    <View style={styles.flatlist_view_outer}>
                        <FlatList
                            style={styles.community_flatliststyle}
                            showsVerticalScrollIndicator={false}
                            nestedScrollEnabled={true}
                            data={Data}
                            keyExtractor={item => item.id}
                            renderItem={({ item }) =>
                                <View style={styles.flatlist_view}>
                                    <View style={styles.profile_image_view} >
                                        <Image source={item.image} style={styles.profile_image} />
                                    </View>
                                    <View style={styles.name_text_view} >
                                        <Text style={styles.name_text}>{item.name}</Text>
                                    </View>
                                </View>
                            }
                        />
                    </View>
                </View>
            </View>
            <View style={styles.blanck_view}></View>
                <View style={styles.second_flatlist_main}>
                    <FlatList
                    style={styles.flatliststyle}
                    contentContainerStyle={{ paddingBottom: hp("2%") }}
                    showsVerticalScrollIndicator={false}
                    showsHorizontalScrollIndicator={false}
                    data={list}
                    nestedScrollEnabled={true}
                    renderItem={({ item }) =>
                        <TouchableOpacity style={styles.second_flatlist_touch}>
                            <Text style={styles.second_flat_text} numberOfLines={3}>{item.text}</Text>
                        </TouchableOpacity>
                    } />
                </View>
           
            </LinearGradient>
        </View>
    )
    }

const styles = StyleSheet.create({
    container: {
        height: hp('100%'),
        width: wp('100%'),
        backgroundColor: myThemeColors.black,
    },

    header_view:{
        width: wp('100%'),
        height: hp('8%'),
        flexDirection: "row",
        alignItems:"center",
        justifyContent:"center",
    },
    Drawer_icon:{
        height: hp('7%'),
        width: wp('85%'),
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center", 
        alignSelf: 'center',
    },
    header_img_view: {
        height: hp('7%'),
        width: wp('12%'),
        justifyContent: "center"
    },
    header_img: {
        height: hp('3%'),
        width: hp('3%'),
        resizeMode: "contain"
    },
    logo_view: {
        height: hp('7%'),
        width: wp('61%'),
        justifyContent: "center",
        alignItems: "center"
    },
    logo: {
        height: hp('13%'),
        width: wp('26%'),
        resizeMode: "contain"
    },
    header_image_view: {
        height: hp('7%'),
        width: wp('12%'),
        justifyContent: "center",
        alignItems: 'flex-end'
    },
    header_image: {
        height: hp('5%'),
        width: hp('5%'),
        resizeMode: "contain"
    },

    image_outer_view:{
        height:hp('26%'),
        width:wp('100%'),
        alignItems:"center",
    },  
    outer_img_text:{
         height:hp('5%'),
         width:wp('85%'),
         justifyContent:"center",
    },  
    on_desk_text:{
        fontFamily:myFontSize.h5.fontFamily,
        fontSize:myFontSize.h6.fontSize,
        color:"#ffffff",
    },
    image_main_view:{
        height:hp('20'),
        width:wp('85%'),
        backgroundColor:"blue",
    },  
    mask_img_view:{ 
        height:hp('22%'),
        width:wp('85%'),
        justifyContent:"flex-end"
    },
    mask_inner_view:{
        height:hp('5%'),
        width:wp('85%'),
        marginLeft:hp('3%'),
    },
    mask_inner_text:{
        color:"#ffffff",
        fontFamily:myFontSize.h8.fontFamily,
        fontSize:myFontSize.h6.fontSize,
    },
    main_linear:{
        height:hp('100%'),
        width:wp('100%'),
    },
    linear:
    {
        marginTop:hp('1.5%'),
        width: wp('85%'),
        height: hp('7%'),
        alignSelf: "center",
        justifyContent: "center",
    },
    linear_button_style:
    {
        width: wp('85%'),
        height: hp('6%'),
        justifyContent: "center",
        alignItems: "center",
    },
    linearstyle:
    {
        borderRadius: hp("7%") / 2,
    },
    next_text:
    {
        color: "#fff",
        fontFamily:myFontSize.h7.fontFamily,
        fontSize: hp("2%"),
    },
    streak_point_outer_view:{

        height:hp('17%'),
        width:wp('100%'),
        alignItems:"center",
    },  
    streak_point_view:{
        height:hp('15%'),
        width:wp('85%'),
        justifyContent:"center",
        flexDirection:"row",
        alignItems:"center",
        borderBottomWidth:1,
        borderBottomColor:"#000000"
    },  
    streak_view:{
        height:hp('10%'),
        width:wp('40%'),
        justifyContent:"center",
        alignItems:"center",
        borderRightWidth:1,
        borderRightColor:"#000000"
    },
    streak_text:{
        fontFamily:'RevolutionGothic_ExtraBold',
        fontSize:myFontSize.h7.fontSize,
        color:"#9D9D9D",  
    },  
    hundred_view:{
        height:hp('5.2%'),
    },
    point_view:{
        height:hp('15%'),
        width:wp('40%'),
        alignItems:"center",
        justifyContent:"center"
    },  
    point_first_text:{
        fontFamily:'RevolutionGothic_ExtraBold',
        fontSize:myFontSize.h7.fontSize,
        color:"#9D9D9D",
    },
    point_second_text:{
        fontFamily:myFontSize.h3.fontFamily,
        fontSize:hp('6%'),
        lineHeight:hp('6.5%'),
        fontWeight:"900",
        color:"#D2D2D2"
    },
    point_third_text:{
        fontFamily:myFontSize.h3.fontFamily,
        fontSize:myFontSize.h8.fontSize,
        color:"#9D9D9D",
    },
    streak_number_view:{ 
        flexDirection:"row",
    },
    fire_img:{
        height:hp('6%'),
        width:wp('6%'),
    },
    text_two_view:{
        fontFamily:myFontSize.h3.fontFamily,
        fontSize:hp('6%'),
        lineHeight:hp('7%'),
        fontWeight:"900",
        color:"#D2D2D2"        
    },  
    first_flatlsit_main:{
        height:hp('16%'),
        width:wp('100%'),
        alignItems:"center",
    },
    first_flatlsit_inner:{
        height:hp('16%'),
        width:wp('85%'),
    },  
    flat_first_text:{
        height:hp('3%'),
        width:wp('85%'),
    },
    first_text_style:{
        fontFamily:myFontSize.h8.fontFamily,
        fontSize:myFontSize.h4.fontSize,
        color:"#ffffff",
    },  
    flatlist_view_outer:{
        height:hp('15%'),
        width:wp('85%'),
    },
    flatlist_view:
    {
        width: wp('85%'),
        height: hp('7.4%'),
        flexDirection: "row",
        alignSelf: "center",
    },
    profile_image_view:
    {
        width: wp('14%'),
        height: hp('7%'),
        justifyContent: "center"
    },
    profile_image:
    {
        width: wp('10.5%'),
        height: hp('5.5%'),
        resizeMode:"contain"
    },
    name_text_view:
    {
        width: wp('65%'),
        height: hp('7%'),
        justifyContent: "center"
    },
    name_text:
    {
        fontSize:myFontSize.h7.fontSize,
        color: myThemeColors.gray_one,
       fontFamily:myFontSize.h3.fontFamily
    },
    second_flatlist_main:{
        height: hp('26%'),
        width: wp('85%'),
        alignSelf: "center",
    },
    second_flatlist_touch:{
        width: wp("85%"),
        height: hp('21%'),
        justifyContent:'center',
        marginBottom: hp('2.3%'),
        borderWidth: hp('0.15%'),
        borderRadius: hp("1%"),
        backgroundColor: myThemeColors.light_gray,
    },
    second_flat_text:{
        fontSize: myFontSize.h6.fontSize,
        fontFamily: myFontSize.h9.fontFamily,
        fontWeight:"bold" ,
        lineHeight:hp('3%'),
        textAlign:"center", 
        color: myThemeColors.white,
    },
    blanck_view:
    {
        height:hp("2.3%"),
        width:wp("85%")
    }

}
)