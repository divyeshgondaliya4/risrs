import React, { useState, useCallback, useRef } from 'react';
import {
    View,
    Text,
    Image,
    StyleSheet,
    StatusBar,
    ScrollView,
    TouchableOpacity,
    TextInput,
    Keyboard,
    ImageBackground,
    Dimensions

} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import myThemeColors from '../utils/myThemeColors';
import myFontSize from '../utils/myFontSize';
import LinearGradient from 'react-native-linear-gradient';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
let { height, width } = Dimensions.get("window")
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;


const OnNextButtonPress = () => {

}

export default function SignUp3(props) {
    const [list, Setlist] = useState();
    return (
        <KeyboardAwareScrollView style={styles.container} extraHeight={hp('15%')} showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false}>
        <ScrollView style={styles.container} >
            <View style={styles.header_view} >
                <TouchableOpacity onPress={() => { props.navigation.goBack() }} style={styles.lefft_eerow_icon_view}>
                    <Image source={require("../assets/icon/BackButton.png")} style={styles.lefft_errow_icon} />
                </TouchableOpacity>
                <View style={styles.header_text_view} >
                    <Text style={styles.header_text}>REGISTER FOR RISR</Text>
                </View>
            </View>
            <View style={styles.progress_bar_view}>
                <View style={styles.progres_bar_style}></View>
                <View style={styles.progres_bar_style1}></View>
                <View style={styles.progres_bar_style1}></View>
                <View style={styles.progres_bar_style2}></View>
                <View style={styles.progres_bar_style2}></View>
            </View>
            {/* <View style={styles.profile_view1} > */}
                <View style={styles.profile_view2}>
                    <View style={styles.profile_view}>
                        <ImageBackground source={require('../assets/image/Mask_Group.png')} resizeMode="contain" style={{ height: hp('20.6%'), width: wp('40%') }}>
                            <TouchableOpacity style={{ alignItems: "center", justifyContent: "center", height: hp('20.6%'), width: wp('40%'),paddingTop:hp("3%") }}>
                                <Image source={require('../assets/image/AddPhoto.png')} resizeMode="contain" style={{ height: hp('10%'), width: wp('20%') }} />
                            </TouchableOpacity>
                        </ImageBackground>

                    </View>
                </View>
            {/* </View> */}
            <View style={styles.main_text_view}>
                <Text style={styles.main_text}>PERSONAL DETAILS</Text>

            </View>
            <View style={styles.text_view}>
                <Text style={styles.text}>Lorem ipsum dolor sit amet, consectetur</Text>
                <Text style={styles.text}>adipiscing elit, sed do eiusmod tempor.</Text>
            </View>
            <View style={styles.text_input_view}>
                <TextInput style={{ color: "#c6c6c6", fontSize: hp('2%') }}
                    placeholder="Full Name"
                    placeholderTextColor={myThemeColors.light_gray}
                />
            </View>
            <View style={styles.text_input_view}>
                <TextInput style={{ color: "#ffffff", fontSize: hp('2%') }}
                    placeholder="Date Of Birth"
                    placeholderTextColor={myThemeColors.light_gray}
                />
            </View>
            <View style={styles.text_input_view}>
                <TextInput style={{ color: "#ffffff", fontSize: hp('2%') }}
                    placeholder="Zip Code"
                    placeholderTextColor={myThemeColors.light_gray}
                />
            </View>
            <View style={styles.text_input_view}>
                <TextInput style={{ color: "#ffffff", fontSize: hp('2%') }}
                    placeholder="Phone Number"
                    placeholderTextColor={myThemeColors.light_gray}
                />
            </View>

            <TouchableOpacity style={[styles.text_input_view, { flexDirection: 'row' }]}>
                <View style={{ height: hp('7%'), width: wp("15%") }}></View>
                <View style={{ height: hp('7%'), width: wp("55%"), alignItems: 'center', justifyContent: 'center' }}>
                    <Text style={{ color: "#C6C6C6" }}>Difficulty : Intermediate</Text>
                </View>
                <View style={{ height: hp('7%'), width: wp("15%"), alignItems: 'center', justifyContent: 'center' }}>
                    <Image source={require("../assets/icon/DownButton.png")} style={[styles.lefft_errow_icon, { height: hp('2%'), width: wp('4%') }]} resizeMode='contain' />
                </View>

            </TouchableOpacity>

            <TouchableOpacity style={[styles.text_input_view, { flexDirection: 'row' }]}>
                <View style={{ height: hp('7%'), width: wp("15%") }}></View>
                <View style={{ height: hp('7%'), width: wp("55%"), alignItems: 'center', justifyContent: 'center' }}>
                    <Text style={{ color: "#C6C6C6" }}>Height(Feet-Inches)</Text>
                </View>
                <View style={{ height: hp('7%'), width: wp("15%"), alignItems: 'center', justifyContent: 'center' }}>
                    <Image source={require("../assets/icon/DownButton.png")} style={[styles.lefft_errow_icon, { height: hp('2%'), width: wp('4%') }]} resizeMode='contain' />
                </View>

            </TouchableOpacity>

            <TouchableOpacity style={[styles.text_input_view, { flexDirection: 'row' }]}>
                <View style={{ height: hp('7%'), width: wp("15%") }}></View>
                <View style={{ height: hp('7%'), width: wp("55%"), alignItems: 'center', justifyContent: 'center' }}>
                    <Text style={{ color: "#C6C6C6" }}>Bats:RH/LH/S</Text>
                </View>
                <View style={{ height: hp('7%'), width: wp("15%"), alignItems: 'center', justifyContent: 'center' }}>
                    <Image source={require("../assets/icon/DownButton.png")} style={[styles.lefft_errow_icon, { height: hp('2%'), width: wp('4%') }]} resizeMode='contain' />
                </View>

            </TouchableOpacity>


            <View style={styles.linear}>
                <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
                    colors={['#C170F3', '#64D2FF',]}
                    style={styles.linearstyle}
                >
                    <TouchableOpacity onPress={() => { props.navigation.navigate('RISR_SignUp_Step4') }} style={styles.linear_button_style}><Text style={styles.next_text}>NEXT</Text></TouchableOpacity>

                </LinearGradient>
            </View>

        </ScrollView>
        </KeyboardAwareScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        height: hp('100%'),
        width: wp('100%'),
        backgroundColor: myThemeColors.black,

    },
    header_view:
    {
        width: wp('90%'),
        height: hp('8%'),
        flexDirection: "row",
        alignSelf: "center",
        justifyContent: "center"
    },
    lefft_eerow_icon_view:
    {
        width: wp('19%'),
        height: hp('8%'),
        justifyContent: "center",


    },
    lefft_errow_icon:
    {
        width: wp('6%'),
        height: hp('3%'),
    },
    header_text_view:
    {
        width: wp('71%'),
        height: hp('8%'),
        justifyContent: "center",



    },
    header_text: {

        fontSize: hp("3%"),
        color: myThemeColors.white,
        fontFamily: 'RevolutionGothic_ExtraBold',
        marginLeft: hp('3%'),

    },
    progress_bar_view: {
        width: wp('85%'),
        height: hp('10%'),
        // backgroundColor: "#fff"
    },
    linear:
    {
        width: wp('85%'),
        height: hp('12%'),
        // position:"absolute",
        // bottom:0,
        // marginBottom:hp("10%"),
        alignSelf: "center",
        justifyContent: "center",

    },
    linear_button_style:
    {
        width: wp('85%'),
        height: hp('7%'),
        justifyContent: "center",
        alignItems: "center",

    },
    linearstyle:
    {
        borderRadius: hp("7%") / 2,
    },
    next_text:
    {
        color: "#fff",
        fontSize: hp("2.3%"),
        fontFamily: 'RevolutionGothic_ExtraBold'
    },
    profile_view1: {
        height: hp("22%"),
        width: wp("100%"),
        // borderRadius:hp("0%"),
        alignItems: "center",
        marginTop: hp('3.5%'),
        // backgroundColor:"red",
        justifyContent: "center"
    },
    profile_view2 : {
        height:hp("20.6%"),
        width:wp("100%"),
        // borderRadius:hp("0%"),
        alignItems:"center", 
        marginTop:hp('2.5%'), 
        justifyContent:"center"
    },
    profile_view : {
        borderRadius: Math.round(Dimensions.get('window').width + Dimensions.get('window').height) / 2,
        width: Dimensions.get('window').width * 0.45,
        height: Dimensions.get('window').width * 0.45,
        backgroundColor:myThemeColors.sky_blue,
        justifyContent: 'center',
        alignItems: 'center'
        
        
    },
    main_text_view: {
        height: hp('10%'),
        width: wp("85%"),
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        // marginTop:hp('2%'),
        // marginVertical:hp('0.5%')

    },
    main_text: {
        color: myThemeColors.white,
        fontFamily: myFontSize.h8.fontFamily,
        fontSize: hp('5.5%')
    },
    text_view: {
        height: hp('3%'),
        width: wp('80%'),
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: hp('1.5%'),
        marginTop: hp('1%')
    },
    text: {
        color: myThemeColors.white,
        fontFamily: myFontSize.h9.fontFamily,
        fontSize: myFontSize.h9.fontSize
    },
    text_input_view: {
        height: hp('7%'),
        width: wp('85%'),
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: hp('4%'),
        borderColor: myThemeColors.white,
        borderWidth: hp('0.1%'),
        marginTop: hp('3%'),
        backgroundColor: myThemeColors.light_black
    },
    progress_bar_view: {
        width: wp('85%'),
        height: hp('7.5%'),
        alignSelf: "center",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center"
        // backgroundColor: "#fff"
    },
    progres_bar_style:
    {
        width: hp("8.5%"),
        height: hp("0.5%"),
        backgroundColor: "#64D2FF",
        borderRadius: hp("0.5%") / 2
    },
    progres_bar_style1:
    {
        width: hp("8.5%"),
        height: hp("0.5%"),
        marginLeft: wp("1%"),
        backgroundColor: "#64D2FF",
        borderRadius: hp("0.5%") / 2
    },
    progres_bar_style2:
    {
        width: hp("8.5%"),
        height: hp("0.5%"),
        marginLeft: wp("1%"),
        backgroundColor: "#3A6F84",
        borderRadius: hp("0.5%") / 2
    },


})