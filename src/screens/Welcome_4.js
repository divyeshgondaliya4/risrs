import Carousel from 'react-native-carousel-view';
import React, { useState, useCallback, useRef } from 'react';
import {
    View,
    Text,
    Image,
    StyleSheet,
    StatusBar,
    ImageBackground,
    TouchableOpacity,
    TextInput,
    Keyboard,
    Dimensions
} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import myThemeColors from '../utils/myThemeColors';
import PagerView from 'react-native-pager-view';
import myFontSize from '../utils/myFontSize';
import AppIntroSlider from 'react-native-app-intro-slider';
import LinearGradient from 'react-native-linear-gradient';
import { Bg_img, Riser_purple_logo } from '../utils/appContant';
let { height, width } = Dimensions.get("window")
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;


export default function Welcome(props) {


    return (
        <View style={styles.container}>
            <ImageBackground source={Bg_img} resizeMode="stretch" style={styles.img_bg}>
            <View style={styles.container}>
                <View style={styles.risr_view}>
                    <Text style={styles.risr_text}>If YOU HAVE A </Text>
                    <Text style={styles.risr_text}>HITTRAX </Text>
                    <Text style={styles.risr_text}> ACCOUNT</Text>
                </View>

                <View style={styles.linear}>
                    <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
                        colors={['#C170F3', '#C170F3',]}
                        style={styles.linearstyle}
                    >
                        <TouchableOpacity onPress={() => { props.navigation.navigate('Home_1') }} style={styles.linear_button_style}><Text style={styles.next_text}>DONE</Text></TouchableOpacity>

                    </LinearGradient>
                </View>
                <View style={styles.circle_view}>
                <TouchableOpacity onPress={() => { props.navigation.navigate('Welcome_1') }}  >
                    <View  style={styles.circle_two}>
                    </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => { props.navigation.navigate('Welcome_2') }}  >
                    <View  style={styles.circle_two}>
                    </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => { props.navigation.navigate('Welcome_3') }}  >
                    <View  style={styles.circle_two}>
                    </View>
                     </TouchableOpacity>
                    
                    <View  style={styles.circle_one}>
                    </View>
                    
                </View>
</View>
            </ImageBackground>
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        height: hp('100%'),
        width: wp('100%'),
        backgroundColor:"#000000c0", 
     },
    img_bg: {
        height: hp("100%"),
        width: wp('100%'), 
    },
    risr_text:
    {
        color: myThemeColors.white,
        fontSize: myFontSize.h12.fontSize,
        fontFamily: myFontSize.h10.fontFamily,
        // paddingTop: hp("6%")

    },
    risr_view:
    {
        height: hp("60%"),
        width: wp("75%"),
        justifyContent: "center",
        alignItems: "center",
        alignSelf:"center",
        paddingTop: hp("20%")

    },
    linear:
    {
        width: wp('85%'),
        height: hp('33%'),
        paddingTop: hp("25%"),
        alignSelf: "center",
        justifyContent: "center",
        // backgroundColor:"yellow"

    },
    linear_button_style:
    {
        width: wp('85%'),
        height: hp('7%'),
        justifyContent: "center",
        alignItems: "center",

    },
    linearstyle:
    {
        borderRadius: hp("7%") / 2,
    },
    next_text:
    {
        color: "#fff",
        fontFamily: 'RevolutionGothic_ExtraBold',
        fontSize: myFontSize.h4.fontSize,

    },
    circle_view:
    {
        width: wp('100%'),
        height: hp('3%'),
        justifyContent:"center",
        alignItems:"center",
        flexDirection:"row"
    },
    circle_one:
    {
        width: wp('2.6%'),
        height: hp('1.5%'),
        borderRadius:hp('1.3%'),
        backgroundColor:"#64D2FF",
        marginLeft:wp("1%")
    },
    circle_two:
    {
        width: wp('2.6%'),
        height: hp('1.5%'),
        borderRadius:hp('1.3%'),
        backgroundColor:"#396577",
        marginLeft:wp("1%")
    },
});