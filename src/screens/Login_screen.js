import React from 'react';
import {
    View,
    Text,
    Image,
    StyleSheet,
    StatusBar,
    TouchableOpacity,
    TextInput,
    Defs,
    Pattern,
    FlatList,
    useWindowDimensions,
    ScrollView,
    ImageBackground

} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import myFontSize from '../utils/myFontSize';
import LinearGradient from 'react-native-linear-gradient';

export default function Login_screen(props) {
    return (
        <KeyboardAwareScrollView style={styles.container} extraHeight={hp('15%')} showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false}>
        <View style={ styles.mainView}>
            <ImageBackground  source={require('../assets/image/bg_photo.jpg')} resizeMode="stretch" style={ styles.bg_Image}>
            <View style={styles.bg_color}>

            
                <View style={styles.logo_outerview}>
                    <View style={styles.logo_innerview}>
                        <Image source={require('../assets/image/logo-riser_purple.png')} resizeMode='contain' style={styles.logo}/>
                    </View>
                </View>
                <View style={styles.second_contain}>
                    <View style={styles.htlogintag}>
                        <View style={styles.htview}>
                            <Image source={require('../assets/image/HT_logo.png')} resizeMode='contain' style={styles.htlogo}/>
                        </View> 
                        <View style={styles.labalview}>
                            <Text style={styles.labaltext}>
                                LOGIN WITH HITTRAX ACCOUNT
                            </Text>
                        </View>
                    </View>
                    <View style={styles.orview}>
                            <Text style={styles.ortext}>
                                OR
                            </Text>
                    </View>
                    <View style={styles.userview}>
                        <TextInput style={styles.all_lable_color}
                             placeholder="Username"
                             placeholderTextColor="#C6C6C6"
                        />
                    </View>
                    <View style={styles.passwordview}>
                        <TextInput style={styles.all_lable_color}
                        placeholder="Password"
                        placeholderTextColor="#C6C6C6"
                        />
                    </View>

                    {/* <View style={styles.singin_view}>
                        <TouchableOpacity style={styles.sigin_touch}>
                        <Text style={styles.all_lable_color_View}>
                                SIGN IN
                        </Text>
                        </TouchableOpacity>
                    </View> */}
                    <View style={styles.linear}>
                        <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
                            colors={['#C170F3', '#64D2FF',]}
                            style={styles.linearstyle}
                        >
                            <TouchableOpacity onPress={()=>{props.navigation.navigate('Home_1')}} style={styles.linear_button_style}><Text style={styles.next_text}>SIGN IN</Text></TouchableOpacity>

                        </LinearGradient>
                    </View>

                    <View style={styles.noaccount_View}>
                        <Text style={styles.noaccount_text}>
                                No account?
                        </Text>
                      
                        <TouchableOpacity onPress={()=>{props.navigation.navigate('RISR_Signup')}}  style={styles.regi_touch}>
                            <Text style={styles.regi_text}>
                                Register now
                            </Text>
                        </TouchableOpacity>
                       

                    </View>
                </View>
                </View>
            </ImageBackground>
        </View>
        </KeyboardAwareScrollView>
    )
}
const styles = StyleSheet.create({
    container:{
        height: hp('100%'),
        width: wp('100%'),
        // paddingTop: hp('3%')
    },
    mainView:{
        height:hp('100%'),
        width:wp('100%'),
    },
    bg_Image:{
        height:hp('100%'),
        width:wp('100%'),
    },
    bg_color:{
        backgroundColor:"#000000c0",
        height:hp('100%'),
        width:wp('100%')

    },

    logo_outerview:{
        marginTop:hp('17%'),
        height:hp('30%'),
        width:wp('100%'),
        alignItems:"center",
    },
    logo_innerview:{
        height:hp('30%'),
        width:wp('50%'),
        justifyContent:"center",
        alignItems:"center",
    },
    logo:{
        height:hp('22%'),
        width:wp('38%'),
    },
    second_contain:{
        height:hp('40%'),
        width:wp('100%'),
        alignItems:"center",
        marginTop:hp('5%'),
    },
    htlogintag:{
        height:hp('7%'),
        width:wp('85%'),
        backgroundColor:"white",
        borderRadius:hp('4%'),
        flexDirection:"row",
        alignItems:"center",
    },
    htview:{ 
        height:hp('7%'),
        width:wp('10%'),
        marginLeft:hp('4%'),
        justifyContent:"center",
        alignItems:"center",
    },
    htlogo:{
        height:hp('5%'),
        width:wp('10%'),
    },
    labalview:{ 
        height:hp('7%'),
        width:wp('65%'),
        alignSelf:"center",
        justifyContent:"center",
        marginLeft:hp('1%'),
    },
    labaltext:{
        fontSize:myFontSize.h2.fontSize,
        fontFamily:'RevolutionGothic_ExtraBold',
        lineHeight:myFontSize.h2.LineHeight,

    },
    orview:{
        height:hp('6%'),
        width:wp('85%'),
        borderRadius:hp('4%'),
        alignItems:"center",
        justifyContent:"center",
    },
    ortext:{
         //fontSize:hp('2%'),
         fontFamily:myFontSize.h1.fontFamily,
         fontSize:myFontSize.h1.fontSize,
         color:"#C6C6C6",
    },
    userview:{
        height:hp('7%'),
        width:wp('85%'),
        borderRadius:hp('4%'),
        alignItems:"center",
        justifyContent:"center",
        color:"transparent",
        borderWidth:hp('0.1%'),
        borderColor:"#fff"
    },
    passwordview:{
        height:hp('7%'),
        width:wp('85%'),
        borderRadius:hp('4%'),
        alignItems:"center",
        justifyContent:"center",
        color:"transparent",
        borderWidth:hp('0.1%'),
        borderColor:"#fff",
        marginTop:hp('2%'),
    },
    all_lable_color:{ 

        color:"#ffffff",
        fontSize:hp('2%'),
        fontFamily:myFontSize.h4.fontFamily,
    },
    noaccount_text:{
        color:"#C6C6C6",
      //  fontSize:hp('2%'),
        fontFamily:myFontSize.h3.fontFamily,
        fontSize:myFontSize.h3.fontSize,
    },
    singin_view:{
        height:hp('7%'),
        width:wp('85%'),
        borderRadius:hp('4%'),
        alignItems:"center",
        justifyContent:"center",
        backgroundColor:"#262626",
        marginTop:hp('2%')
    },
    sigin_touch:{
        height:hp('7%'),
        width:wp('85%'),
        alignItems:"center",
        justifyContent:"center",
    },
    noaccount_View:{
        height:hp('7%'),
        width:wp('85%'),
        borderRadius:hp('4%'),
        alignItems:"center",
        justifyContent:"center",
        // marginTop:hp('2%'),
        flexDirection:"row",
    },
    regi_touch:{
        height:hp('3%'),
        width:wp('28%'),
        borderBottomWidth:1,
        borderBottomColor:"white",
    },
    regi_text:{
        color:"#ffffff",
        marginLeft:hp('0.6%'),
          fontSize:hp('2%'), 
        fontFamily:myFontSize.h4.fontFamily,
        //fontSize:myFontSize.h4.fontSize,
    },
    all_lable_color_View:{
        fontFamily:myFontSize.h2.fontFamily,
        fontSize:myFontSize.h2.fontSize,
        // fontWeight:myFontSize.h2.fontWidth,
        lineHeight:myFontSize.h2.lineHeight,
        color:"#fff"
    },
    linear:
    {

        marginTop:hp('1%'),
        width: wp('85%'),
        height: hp('10%'),
        // position:"absolute",
        // bottom:0,
        // marginBottom:hp("10%"),
        alignSelf: "center",
        justifyContent: "center"
    
    },
    linear_button_style:
    {
        width: wp('85%'),
        height: hp('7%'),
        justifyContent: "center",
        alignItems: "center",

    },
    linearstyle:
    {
        borderRadius: hp("7%") / 2,
    },
    next_text:
    {
        color: "#fff",
        fontFamily:'RevolutionGothic_ExtraBold',
        fontSize: hp("3%"),
    },



})