import React from 'react';
import {
    View,
    Text,
    Image,
    StyleSheet,
    StatusBar,
    TouchableOpacity,
    TextInput,
    Defs,
    Pattern,
    FlatList,
    useWindowDimensions,
    ScrollView,
    ImageBackground

} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import myFontSize from '../utils/myFontSize';
import LinearGradient from 'react-native-linear-gradient';
import { Menu_icon, Risr, Ellipse_9, Mask_Group_bg, Fire } from '../utils/appContant';
import myThemeColors from '../utils/myThemeColors';


export default function Home(props) {
    return (

        <View style={styles.mainView}>
            <View style={styles.header_view}>
                <View style={styles.header_img_view}>
                    <TouchableOpacity><Image style={styles.header_img} source={Menu_icon}></Image></TouchableOpacity>
                </View>
                <View style={styles.logo_view}>
                    <TouchableOpacity><Image style={styles.logo} source={Risr}></Image></TouchableOpacity>
                </View>
                <View style={styles.header_image_view}>
                    <TouchableOpacity><Image style={styles.header_image} source={Ellipse_9}></Image></TouchableOpacity>
                </View>
            </View>
            <View style={styles.lesson_view}>
                <Text style={styles.lesson_text}>Lessons</Text>
            </View>
            <View style={styles.tab_view}>
                <View style={styles.tab_one}>
                    <Text style={styles.hitting_text}>HITTING</Text>
                </View>
                <View style={styles.tab_two}>
                    <Text style={styles.mental_text}>MENTAL TRAINING</Text>
                </View>
            </View>

        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        height: hp('100%'),
        width: wp('100%'),
        // paddingTop: hp('3%')
    },
    mainView: {
        height: hp('100%'),
        width: wp('100%'),
        backgroundColor: myThemeColors.black_one,
    },
    header_view: {
        height: hp('10%'),
        width: wp('85%'),
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        alignSelf: 'center'
    },
    header_img_view: {
        height: hp('7%'),
        width: wp('12%'),
        justifyContent: "center"
    },
    header_img: {
        height: hp('3%'),
        width: hp('3%'),
        resizeMode: "contain"
    },
    logo_view: {
        height: hp('7%'),
        width: wp('61%'),
        justifyContent: "center",
        alignItems: "center"
    },
    logo: {
        height: hp('11%'),
        width: wp('22%'),
        resizeMode: "contain"
    },
    header_image_view: {
        height: hp('7%'),
        width: wp('12%'),
        justifyContent: "center",
        alignItems: 'flex-end'
    },
    header_image: {
        height: hp('5%'),
        width: hp('5%'),
        resizeMode: "contain"
    },
    lesson_view:
    {
        height: hp('12%'),
        width: wp('85%'),
        // backgroundColor:"yellow",
        alignSelf: "center",
      
    },
    lesson_text:
    {
        color: myThemeColors.white,
        fontSize: myFontSize.h10.fontSize,
        fontFamily: myFontSize.h10.fontFamily
    },
    tab_view:
    {
        height: hp('7%'),
        width: wp('85%'),
        flexDirection:"row",
        alignSelf:"center",
        borderWidth:1,
        borderColor:myThemeColors.white,
    },
    tab_one:
    {
        height: hp('7%'),
        width: wp('42.5%'),
        justifyContent:"center",
        alignItems:"center",
    },
    hitting_text:
    {
        color: myThemeColors.white,
        fontSize: myFontSize.h11.fontSize,
        fontFamily: myFontSize.h3.fontFamily
    },
    tab_two:
    {
        height: hp('7%'),
        width: wp('42.5%'),
        justifyContent:"center",
        alignItems:"center",
    },
    mental_text:
    {
        color: myThemeColors.white,
        fontSize: myFontSize.h11.fontSize,
        fontFamily: myFontSize.h3.fontFamily
    }

});