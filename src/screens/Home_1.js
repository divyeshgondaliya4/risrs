import React from 'react';
import {
    View,
    Text,
    Image,
    StyleSheet,
    StatusBar,
    TouchableOpacity,
    TextInput,
    Defs,
    Pattern,
    FlatList,
    useWindowDimensions,
    ScrollView,
    ImageBackground,
    Dimensions

} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import myFontSize from '../utils/myFontSize';
import LinearGradient from 'react-native-linear-gradient';
import { Menu_icon, Risr, Ellipse_9, Mask_Group_bg, Fire } from '../utils/appContant';
import myThemeColors from '../utils/myThemeColors';
let { height, width } = Dimensions.get("window")
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;


const Data = [
    {
        id: 1,
        text: "HITTING LESSONS",
    },
    {
        id: 2,
        text: "MENTAL TRAINNING LESSONS",
    },
    {
        id: 3,
        text: "HITTING LESSONS",
    },
    {
        id: 4,
        text: "MENTAL TRAINNING LESSONS",
    },
    {
        id: 5,
        text: "MENTAL TRAINNING LESSONS",
    },
];

export default function Home(props) {
    return (
        // <KeyboardAwareScrollView style={styles.container} extraHeight={hp('15%')} showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false}>
        <View style={styles.mainView}>
            <LinearGradient start={{ x: 0, y: 1 }} end={{ x: 0, y: -2}}
                colors={['#1F1F1F', '#000000',]}
                style={styles.linearstyle1}
            >
            <View style={styles.header_view}>
                <View style={styles.header_img_view}>
                    <TouchableOpacity><Image style={styles.header_img} source={Menu_icon}></Image></TouchableOpacity>
                </View>
                <View style={styles.logo_view}>
                   <Image style={styles.logo} source={Risr}></Image>
                </View>
                <View style={styles.header_image_view}>
                    <TouchableOpacity><Image style={styles.header_image} source={Ellipse_9}></Image></TouchableOpacity>
                </View>
            </View>

            <View style={styles.bg_img}>
                <View style={styles.bg_text_view}>
                    <Text style={styles.bg_text}>Where You Should Start</Text>
                </View>
                <View style={styles.img_view}>
                    <ImageBackground source={Mask_Group_bg} resizeMode="stretch" style={styles.back_img}>
                        <View style={styles.back_img_text}>
                            <Text style={styles.text}>Lesson/Program title</Text>
                        </View>
                    </ImageBackground>
                </View>
            </View>

            <View style={styles.streak_point_outer_view}>
                <View style={styles.streak_point_view}>
                    <View style={styles.streak_view}>
                        <Text style={styles.streak_text}>
                            DAILY STREAK
                        </Text>
                        <View style={styles.streak_number_view}>
                            <Text style={styles.text_two_view}>
                                1
                            </Text>
                            <Image source={Fire} resizeMode="contain" style={styles.fire_img} />
                        </View>
                    </View>
                    <View style={styles.point_view}>
                        <Text style={styles.point_first_text}>
                            NEXT TROPHY IN
                        </Text>
                        <View style={styles. hundred_view}>
                            <Text style={styles.point_second_text}>
                                100
                            </Text>
                        </View>
                        <Text style={styles.point_third_text}>
                            POINTS
                        </Text>
                    </View>
                </View>
            </View>

            <View style={styles.text_one}>
                <View style={styles.text_view}>
                    <Text style={styles.text1}>What Your Friends are Doing</Text>
                </View>
                <View style={styles.box_view}>
                    <View style={styles.box_text_view}>
                        <Text style={styles.box_text}>Add your contacts to find your friends on RISR</Text>
                        {/* <Text style={styles.box_text}></Text> */}
                    </View>
                    <View style={styles.add_view}>
                        <TouchableOpacity onPress={() => { props.navigation.navigate('Home_2') }}><Text style={styles.add_text}>ADD</Text></TouchableOpacity>
                    </View>
                </View>
            </View>
            <View style={styles.blanck_view}></View>
            <View style={styles.flatlist_view}>
                <FlatList
                    style={styles.flatliststyle}
                    contentContainerStyle={{ paddingBottom: hp("5%") }}
                    showsVerticalScrollIndicator={false}
                    showsHorizontalScrollIndicator={false}
                    // vertical={true}
                    data={Data}
                    nestedScrollEnabled={true}
                    //  keyExtractor={item => Object}
                    renderItem={({ item }) =>
                        <TouchableOpacity style={styles.card_view}>
                            <Text style={styles.card_text} numberOfLines={3}>{item.text}</Text>
                        </TouchableOpacity>
                    } />
            </View>

            </LinearGradient>
        </View>
        // </KeyboardAwareScrollView>

    )
}
const styles = StyleSheet.create({
    container: {
        height: hp('100%'),
        width: wp('100%'),
        // paddingTop: hp('3%')
    },
    mainView: {
        height: hp('100%'),
        width: wp('100%'),
        backgroundColor: myThemeColors.black_one,
    },
    header_view: {
        height: hp('8%'),
        width: wp('85%'),
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        alignSelf: 'center'
    },
    header_img_view: {
        height: hp('7%'),
        width: wp('12%'),
        justifyContent: "center"
    },
    header_img: {
        height: hp('3%'),
        width: hp('3%'),
        resizeMode: "contain"
    },
    logo_view: {
        height: hp('7%'),
        width: wp('61%'),
        justifyContent: "center",
        alignItems: "center"
    },
    logo: {
        height: hp('13%'),
        width: wp('26%'),
        resizeMode: "contain"
    },
    header_image_view: {
        height: hp('7%'),
        width: wp('12%'),
        justifyContent: "center",
        alignItems: 'flex-end'
    },
    header_image: {
        height: hp('5%'),
        width: hp('5%'),
        resizeMode: "contain"
    },
    bg_img: {
        height: hp('26%'),
        width: wp('85%'),
        alignSelf: 'center'
    },
    bg_text_view: {
        height: hp('5%'),
        width: wp('85%')
    },
    bg_text: {
        fontSize: myFontSize.h14.fontSize,
        fontFamily: myFontSize.h14.fontFamily,
        color: myThemeColors.white
    },
    img_view: {
        height: hp('21%'),
        width: wp('85%'),
        justifyContent: "center",
        borderRadius:hp("5%")
    },
    back_img: {
        height: hp('22%'),
        width: wp('85%'),
        justifyContent: 'center',
        alignSelf: 'center',
        borderRadius:hp("20%")
    },
    back_img_text: {
        height: hp('5%'),
        width: wp('85%'),
        alignSelf: 'center',
        justifyContent: 'center',
        top: hp('7.5%'),
        paddingLeft: hp('2%')
    },
    text: {
        fontSize: myFontSize.h13.fontSize,
        fontFamily: myFontSize.h13.fontFamily,
        color: myThemeColors.white
    },
    text_one: {
        height: hp('14%'),
        width: wp('85%'),
        alignSelf: 'center',
        justifyContent: 'flex-end',
    },
    text_view: {
        height: hp('5%'),
        width: wp('85%'),
        alignSelf: 'center',
        justifyContent: 'flex-end'
    },
    text1: {
        fontSize: myFontSize.h4.fontSize,
        fontFamily: myFontSize.h13.fontFamily,
        color: myThemeColors.sky_blue,
        bottom: hp('1%')
    },
    box_view: {
        height: hp('9%'),
        width: wp('85%'),
        borderRadius: hp('1%'),
        backgroundColor: myThemeColors.purple_one,
        flexDirection: 'row'
    },
    box_text_view: {
        height: hp('9%'),
        width: wp('57%'),
        justifyContent: 'center',
        paddingLeft: hp('1.7%'),
        paddingRight: hp('1.7%'),
        // backgroundColor:"red"
    },
    box_text: {
        fontSize: myFontSize.h4.fontSize,
        fontFamily: myFontSize.h3.fontFamily,
        lineHeight: 18,
        color: myThemeColors.white,
        marginLeft:hp('2%'),
    },
    add_view: {
        height: hp('9%'),
        width: wp('28%'),
        justifyContent: 'center',
        alignSelf: 'center',
        // backgroundColor:"yellow"
    },
    add_text: {
        borderRadius: hp('3%'),
        borderWidth: hp('0.15%'),
        borderColor: myThemeColors.white,
        paddingLeft: hp('3%'),
        paddingRight: hp('3%'),
        paddingTop: hp('1%'),
        paddingBottom: hp('1%'),
        alignSelf: 'center',
        color: myThemeColors.white,
        fontSize: myFontSize.h4.fontSize,
        fontFamily: myFontSize.h12.fontFamily,
        fontWeight: '800'
    },
    card_view: {
        width: wp("85%"),
        height: hp('21%'),
        justifyContent: 'center',
        marginBottom: hp('2.3%'),
        borderWidth: hp('0.15%'),
        borderRadius: hp("1%"),
        backgroundColor: myThemeColors.light_gray
    },
    card_text: {
        fontSize: myFontSize.h6.fontSize,
        fontFamily: myFontSize.h9.fontFamily,
        textAlign:"center", fontWeight: 'bold',
        color: myThemeColors.white
    },
    streak_point_outer_view: {

        height: hp('17%'),
        width: wp('100%'),
        alignItems: "center",
        // backgroundColor:"red",
    },
    streak_point_view: {
        height: hp('15%'),
        width: wp('85%'),
        justifyContent: "center",
        flexDirection: "row",
        //backgroundColor:"white",
        alignItems: "center",
        borderBottomWidth: 1,
        borderBottomColor: "#000000"
    },
    streak_view: {
        height: hp('10%'),
        width: wp('40%'),
        // backgroundColor:"green",
        justifyContent: "center",
        alignItems: "center",
        borderRightWidth: 1,
        borderRightColor: "#000000"
    },
    streak_text: {
        fontFamily: myFontSize.h7.fontFamily,
        fontSize: myFontSize.h7.fontSize,
        color: "#9D9D9D",

    },
    point_view: {
        height: hp('15%'),
        width: wp('40%'),
        alignItems: "center",
        justifyContent: "center"
    },
    point_first_text: {
        fontFamily: myFontSize.h7.fontFamily,
        fontSize: myFontSize.h7.fontSize,
        color: "#9D9D9D",
    },
    point_second_text: {
        fontFamily: myFontSize.h3.fontFamily,
        fontSize: hp('6%'),
        lineHeight: hp('6.5%'),
        fontWeight: "900",
        color: "#D2D2D2"
    },
    point_third_text: {
        fontFamily: myFontSize.h3.fontFamily,
        fontSize: myFontSize.h8.fontSize,
        color: "#9D9D9D",
        
    },
    streak_number_view: {
        flexDirection: "row",
    },
    fire_img: {
        height: hp('6%'),
        width: wp('6%'),
        // paddingTop:hp("8%")
    },
    text_two_view: {
        fontFamily: myFontSize.h4.fontFamily,
        fontSize: hp('6%'),
        lineHeight: hp('7%'),
        fontWeight: "900",
        color: "#D2D2D2",
       },
    flatlist_view:
    { height: hp('34%'), width: wp('85%'), alignSelf: "center" },
    hundred_view:{
        height:hp('5.2%'),
    },
    blanck_view:
    {
        height:hp("2.3%"),
        width:wp("85%")
    }
})