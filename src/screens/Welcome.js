import Carousel from 'react-native-carousel-view';
import React, { useState, useCallback, useRef } from 'react';
import {
    View,
    Text,
    Image,
    StyleSheet,
    StatusBar,
    ImageBackground,
    TouchableOpacity,
    TextInput,
    Keyboard,
    Dimensions
} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import myThemeColors from '../utils/myThemeColors';
import PagerView from 'react-native-pager-view';
import myFontSize from '../utils/myFontSize';
import AppIntroSlider from 'react-native-app-intro-slider';
import LinearGradient from 'react-native-linear-gradient';
import { Bg_img, Riser_purple_logo } from '../utils/appContant';
let { height, width } = Dimensions.get("window")
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const images = [
    'https://placeimg.com/640/640/nature',
    'https://placeimg.com/640/640/people',
    'https://placeimg.com/640/640/animals',
    'https://placeimg.com/640/640/beer',
];
const slides = [
    {
        key: 1,
        title: 'RISR IS',
        button:'NEXT',
        text: 'Description.\nSay something cool',
        image: require('../assets/image/Mask_Group_bg.png'),
        backgroundColor: '#000000c0',
        style:{
            width: hp('37%'),
        marginTop: hp('30%'),
        marginBottom: hp("60%"),
        fontSize: hp('5.5%'),
        fontFamily: 'RevolutionGothic_ExtraBold',
        color: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        textTransform: 'uppercase',
        textAlign: 'center',
        }
    },
    {
        key: 2,
        button:'NEXT',
        title: 'SCREENSHOT',
        text: 'SCREENSHOT',
        image: require('../assets/image/Mask_Group_bg.png'),
        backgroundColor: '#000000c0',
        style:{
            width: hp('37%'),
        marginTop: hp('30%'),
        marginBottom: hp("60%"),
        fontSize: hp('5.5%'),
        fontFamily: 'RevolutionGothic_ExtraBold',
        color: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        textTransform: 'uppercase',
        textAlign: 'center',
        }
    },
    {
        key: 3,
        title: 'ASSESSMENT',
        button:'NEXT',
        text: 'I\'m already out of descriptions\n\nLorem ipsum bla bla bla',
        image: require('../assets/image/Mask_Group_bg.png'),
        backgroundColor: '#000000c0',
        style:{
            width: hp('37%'),
        marginTop: hp('30%'),
        marginBottom: hp("60%"),
        fontSize: hp('5.5%'),
        fontFamily: 'RevolutionGothic_ExtraBold',
        color: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        textTransform: 'uppercase',
        textAlign: 'center',
        }
    },
    {
        key: 4,
        button:'DONE',
        title: 'If you have a hittrax account',
        text: 'I\'m already out of descriptions\n\nLorem ipsum bla bla bla',
        image: require('../assets/image/Mask_Group_bg.png'),
        backgroundColor: '#000000c0',
        style:{
            width: hp('37%'),
        marginTop: hp('30%'),
        marginBottom: hp("45.05%"),
        fontSize: hp('5.5%'),
        fontFamily: 'RevolutionGothic_ExtraBold',
        color: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        textTransform: 'uppercase',
        textAlign: 'center',
        }
    },

];
export default function Welcome(props) {
    slider: AppIntroSlider | undefined;
    _renderNextButton = () => {
        return (
            <View style={styles.linear}>
            <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
                colors={['#C170F3', '#64D2FF',]}
                style={styles.linearstyle}
            >
                <TouchableOpacity style={styles.linear_button_style}><Text style={styles.next_text}>NEXT</Text></TouchableOpacity>
            </LinearGradient>
        </View>
        );
      };
      _renderDoneButton  = () => {
        return (
            <View style={styles.linear}>
            <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
                colors={['#C170F3', '#64D2FF',]}
                style={styles.linearstyle}
            >
                <TouchableOpacity style={styles.linear_button_style}><Text style={styles.next_text}>DONE</Text></TouchableOpacity>
            </LinearGradient>
        </View>
        );
      };
    _renderItem = ({ item }) => {
        
        return ( 
            <View style={styles.slide}>
                <Text numberOfLines={3} style={item.style}>{item.title}</Text>

                <View style={styles.linear}>
                    <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
                        colors={['#C170F3', '#64D2FF',]}
                        style={styles.linearstyle}
                    >
                        <TouchableOpacity onPress={() => {props.navigation.navigate('Login_screen') }} style={styles.linear_button_style}><Text style={styles.next_text}>{item.button}</Text></TouchableOpacity>
                    </LinearGradient>
                </View>
            </View> 
        );
    }
    return (
        <View style={styles.container} >
            <ImageBackground source={Bg_img} resizeMode="stretch" style={styles.img_bg}>
                <View style={styles.linear}>
                </View>
                <AppIntroSlider  dotStyle={{backgroundColor: '#295567'}} activeDotStyle={{backgroundColor: '#64D2FF'}} renderNextButton={_renderNextButton} renderDoneButton={_renderDoneButton} renderItem={_renderItem} data={slides} showDoneButton={false} showNextButton={false} />

            </ImageBackground>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        height: hp('100%'),
        width: wp('100%'), 
        backgroundColor: '#000000c0',
    },
    slide: { 
        justifyContent:'center',
        alignItems:'center',
        flex: 1, 
        opacity: 1,  
        backgroundColor: '#000000c0',
    },
    linear:
    {
        width: wp('85%'),
        height: hp('20%'),
        // position:"absolute",
         marginTop:hp('-20%')

    },
    next_text:
    {
        color: "#fff", 
        fontSize: hp("3%"),
        fontFamily:'RevolutionGothic_ExtraBold',
    },
    linear_button_style:
    {
        width: wp('85%'),
        height: hp('7%'),
        justifyContent: "center",
        alignItems: "center",
        alignSelf:"center"
    },
    linearstyle:
    {
        alignSelf:'center',
        marginTop:hp('6%'),
        borderRadius: hp("7%") / 2,
    },
    img_bg: {
        height: hp("100%"),
        width: wp('100%'),
        backgroundColor: '#000000c0',
    },
    image: {
        width: wp('100%'),
        height: hp('100%'),
    },
    text: {
        color: 'rgba(255, 255, 255, 0.8)',
        textAlign: 'center',
    },
    title: {
        width: hp('37%'),
        marginTop: hp('9%'),
        marginBottom: hp("42%"),
        fontSize: hp('5.4%'),
        fontFamily: 'RevolutionGothic_ExtraBold',
        color: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        textTransform: 'uppercase',
        textAlign: 'center',
    },
})