import React, { useEffect, useState, useCallback, useRef } from 'react';
import {
    View,
    Text,
    Image,
    StyleSheet,
    StatusBar,
    ImageBackground,
    TouchableOpacity,
    TextInput,
    Keyboard,
} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import myThemeColors from '../utils/myThemeColors';
import myFontSize from '../utils/myFontSize';
import DefaultWelcome from '../screens/DefaultWelcome';
import { Bg_img, Riser_purple_logo } from '../utils/appContant';
import { StackActions, NavigationActions } from 'react-navigation';
const resetAction = StackActions.reset({  
    index:0,
    actions: [
    NavigationActions.navigate({ routeName: 'Welcome_1'})]
  })

export default function Splash(props) {

    useEffect(() => {
        setTimeout(() => {
            props.navigation.replace('Welcome_1');
        }, 2000);
    }, []);
    return (
        <View style={[styles.container, { backgroundColor: myThemeColors.black }]}>
            <StatusBar
                backgroundColor={myThemeColors.black}
            //    barStyle = {"dark-content"}
            />
            <View style={styles.container}>
                <ImageBackground source={Bg_img} resizeMode="stretch" style={styles.img_bg}>
                    <View style={styles.img_view}>
                        <Image
                            source={Riser_purple_logo}
                            style={styles.img}
                        />
                    </View>
                </ImageBackground>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        height: hp('100%'),
        width: wp('100%'),
    },
    img_bg:{
        height: hp("100%"), 
        width: wp('100%'),
    },
    img_view:{
        height: hp('100%'), 
        width: wp('100%'), 
        justifyContent: 'center', 
        alignItems: 'center' 
    },
    img:{
        height: hp("30%"), 
        width: wp('60%'), 
        resizeMode: 'contain'
    }

})