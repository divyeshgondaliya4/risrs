import React, { useState, useEffect } from 'react';
import {
    View,
    Text,
    Image,
    StyleSheet,
    StatusBar,
    TextInput,
    Defs,
    Pattern,
    FlatList,
    useWindowDimensions,
    ScrollView,
    TouchableOpacity,
    Dimensions


} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import myThemeColors from '../utils/myThemeColors';
import myFontSize from '../utils/myFontSize';
import LinearGradient from 'react-native-linear-gradient';
let { height, width } = Dimensions.get("window")
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

export default function SignUp5(props, onTabChange) {

    const [tabDay, settabDay] = React.useState(1);
    const [tabIndex, SettabIndex] = useState(1);

    return (
        <View style={styles.container} >
            <View style={styles.header_view} >
                <TouchableOpacity onPress={() => { props.navigation.goBack() }} style={styles.lefft_eerow_icon_view}>
                    <Image source={require("../assets/icon/BackButton.png")} style={styles.lefft_errow_icon} />
                </TouchableOpacity>
                <View style={styles.header_text_view} >
                    <Text style={styles.header_text}>REGISTER FOR RISR</Text>
                </View>
            </View>
            <View style={styles.progress_bar_view}>
                <View style={styles.progres_bar_style1}></View>
                <View style={styles.progres_bar_style1}></View>
                <View style={styles.progres_bar_style1}></View>
                <View style={styles.progres_bar_style1}></View>
                <View style={styles.progres_bar_style1}></View>
            </View>
            <View style={styles.profile_view2} >
                <View style={styles.profile_view}></View>
            </View>
            <View style={styles.text}>
                <View style={styles.firsttext}>
                    <Text style={styles.text1}>START YOUR</Text>
                    <Text style={styles.text1}>SUBSCRIPTION</Text>
                </View>
            </View>
            <View style={styles.text_view}>
                <Text style={styles.text}>Access unlimited lessons,track progress,earn</Text>
                <Text style={styles.text}>points and archivements</Text>
            </View>

            {tabIndex == 1 ?
                <View style={styles.linear_tab}>
                    <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
                        colors={['#755DE9', '#5E5CE6',]}
                        style={ tabIndex == 1 ?styles.linearstyle_border:styles.monthly}
                    >
                        <TouchableOpacity onPress={() => { SettabIndex(1) }} style={tabIndex == 1 ? styles.linear_monthly : styles.monthly}><Text style={styles.monthly_text}>MONTHLY</Text></TouchableOpacity>
                    </LinearGradient>
                    <TouchableOpacity style={tabIndex == 1 ? styles.linear_Annually : styles.Annually}>
                        <Text style={{ color: myThemeColors.white, fontFamily: myFontSize.h3.fontFamily }}>ANNUALLY</Text>
                    </TouchableOpacity>
                </View>
                :
                <View style={styles.linear_tab}>
                    <TouchableOpacity style={tabIndex == 1 ? styles.linear_Annually : styles.Annually}>
                        <Text style={{ color: myThemeColors.white, fontFamily: myFontSize.h3.fontFamily }}>MONTHLY</Text>
                    </TouchableOpacity>
                    <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
                        colors={['#755DE9', '#5E5CE6',]}
                        style={styles.linearstyle_border}
                    >
                        <TouchableOpacity onPress={() => { SettabIndex(2    ) }} style={tabIndex == 1 ? styles.linear_monthly : styles.monthly}><Text style={styles.monthly_text}>ANNUALLY</Text></TouchableOpacity>
                    </LinearGradient>
                </View>
            }
            {/* </LinearGradient> */}


            <View style={styles.doller}>
                <Text style={{ color: myThemeColors.sky_blue, fontFamily: myFontSize.h4.fontFamily, fontSize: hp('5%'), fontWeight: 'bold' }}>$14.99</Text>
            </View>
            <View style={styles.mainmonth}>
                <Text style={{ color: myThemeColors.white, fontFamily: myFontSize.h4.fontFamily, fontSize: myFontSize.h4.fontSize }}>/MONTH</Text>
            </View>
            <View style={styles.subscription}>
                <Text style={{ color: myThemeColors.white, fontFamily: myFontSize.h4.fontFamily, fontSize: hp('2.3%'), fontWeight: 'bold' }}>Save XX% with annual Subscription</Text>
            </View>
            <View style={styles.linear}>
                <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
                    colors={['#C170F3', '#64D2FF',]}
                    style={styles.linearstyle}
                >
                    <TouchableOpacity style={styles.linear_button_style}><Text style={styles.next_text}>UPGRADE NOW</Text></TouchableOpacity>

                </LinearGradient>
            </View>
            <View style={styles.lasttext}>
                <TouchableOpacity>
                    <Text style={styles.lastborder}>Not right now</Text>
                </TouchableOpacity>
            </View>

        </View>
    )

}

const styles = StyleSheet.create({
    container: {
        height: hp('100%'),
        width: wp('100%'),
        backgroundColor: "#000000"

    },
    header_view:
    {
        width: wp('90%'),
        height: hp('8%'),
        flexDirection: "row",
        alignSelf: "center"
    },
    lefft_eerow_icon_view:
    {
        width: wp('19%'),
        height: hp('8%'),
        justifyContent: "center",
        // backgroundColor:"red"

    },
    lefft_errow_icon:
    {
        width: wp('6%'),
        height: hp('3%'),
    },
    header_text_view:
    {
        width: wp('71%'),
        height: hp('8%'),
        justifyContent: "center",

    },
    header_text: {

        fontSize: hp("3%"),
        color: "#ffffff",
        marginLeft:hp('3%'),
        fontFamily: 'RevolutionGothic_ExtraBold'

    },
    progress_bar_view: {
        width: wp('85%'),
        height: hp('10%'),

        // backgroundColor: "#fff"
    },
    profile_view2: {
        height: hp("21%"),
        width: wp("100%"),
        // borderRadius:hp("0%"),
        alignItems: "center",
        marginBottom:hp('1.5%'), 
        justifyContent: "center"
    },
    profile_view: {
        borderRadius: Math.round(Dimensions.get('window').width + Dimensions.get('window').height) / 2,
        width: Dimensions.get('window').width * 0.45,
        height: Dimensions.get('window').width * 0.45,
        backgroundColor:myThemeColors.gray_one,
        justifyContent: 'center',
        alignItems: 'center'
        

    },
    text: {
        height: hp('15%'),
        width: wp('100%'),
        backgroundColor: 'pink',
        // marginVertical: hp('1%')
    },
    firsttext: {
        height: hp('15%'),
        width: wp("90%"),
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        // marginVertical: hp('0.5%')

    },
    secondtext: {
        height: hp('7.5%'),
        width: wp('80%'),
        alignSelf: 'center',

    },
    text1: {
        color: myThemeColors.white,
        fontFamily: myFontSize.h8.fontFamily,
        fontSize: hp('5.5%'),
        // lineHeight:hp("5%")
    },
    view: {
        height: hp('2%'),
        width: wp('100%'),

    },
    text_view: {
        height: hp('5%'),
        width: wp('85%'),
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        // marginVertical: hp('0.5%'),
        marginBottom: hp('2%')
    },
    text: {
        color: myThemeColors.white,
        fontFamily: myFontSize.h4.fontFamily,
        fontSize: hp('1.8%')
    },
    monthly: {
        height: hp('5.8%'),
        width: wp('42.5%'),
        //   backgroundColor:'yellow',
        alignItems: 'center',
        justifyContent: 'center',
        borderRightWidth: 1,
        borderRightColor: "#ffffff"

    },
    monthly_text:
    {
        color: myThemeColors.white
    },
    annually: {
        height: hp('5.8%'),
        width: wp('42.5%'),
        // backgroundColor:'red',
        alignItems: 'center',
        justifyContent: 'center'
    },
    doller: {
        marginTop: hp('1%'),
        height: hp('6%'),
        width: wp('100%'),
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center'
    },
    mainmonth: {
        height: hp('3%'),
        width: wp('100%'),
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center'
    },
    subscription: {
        height: hp('5%'),
        width: wp('100%'),
        alignItems: 'center',
        marginTop: hp('1.5%')
    },
    lasttext: {
        height: hp('4%'),
        width: wp('100%'),
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center',

    },
    lastborder: {
        borderBottomWidth: hp('0.2%'),
        fontFamily: myFontSize.h4.fontFamily,
        color: myThemeColors.white,
        fontSize: myFontSize.h3.fontSize,
        borderBottomColor: myThemeColors.gray_one,
    },
    monthlyfirstView: {
        borderColor: myThemeColors.white,
        borderWidth: hp('0.2%'),
        marginVertical: hp('0.5%'),
        borderWidth: 1, borderColor: "#FFFFFF",
        height: hp('6%'),
        width: wp('85%'),
        flexDirection: 'row',
        alignSelf: 'center',
        justifyContent: 'center',
        borderRadius: hp('4%'),
    },
    profile: {
        height: hp("19%"),
        width: wp("38%"),
        borderRadius: hp('38%'),
        alignSelf: 'center',
        backgroundColor: '#c4c4c4'
    },
    linear:
    {
        width: wp('85%'),
        height: hp('10%'),
        // position:"absolute",
        // bottom:0,
        // marginBottom:hp("10%"),
        alignSelf: "center",
        justifyContent: "center",

    },
    linear_button_style:
    {
        width: wp('85%'),
        height: hp('7%'),
        justifyContent: "center",
        alignItems: "center",

    },
    linear_tab:
    {
        width: wp('85%'),
        height: hp('5.8%'),
        borderRadius: hp("5%"),
        // position:"absolute",
        // bottom:0,
        // marginBottom:hp("10%"),
        alignSelf: "center",
        justifyContent: "center",
        flexDirection: "row",
        borderWidth: 1,
        borderColor: "#fff"

    },
    linear_monthly:
    {
        width: wp('42%'),
        height: hp('5.8%'),
        justifyContent: "center",
        alignItems: "center",
        borderRightWidth: 1,
        borderRightColor: "#fff"
    },
    linearstyle:
    {
        borderRadius: hp("7%") / 2,
    },
    next_text:
    {
        color: "#fff",
        fontFamily: 'RevolutionGothic_ExtraBold',
        fontSize: myFontSize.h4.fontSize,

    },

    progress_bar_view: {
        width: wp('85%'),
        height: hp('7.5%'),
        alignSelf: "center",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        marginBottom:hp("1.5%")
        // backgroundColor: "#fff"
    },
    progres_bar_style:
    {
        width: hp("8.5%"),
        height: hp("0.5%"),
        backgroundColor: "#64D2FF",
        borderRadius: hp("0.5%") / 2
    },
    progres_bar_style1:
    {
        width: hp("8.5%"),
        height: hp("0.5%"),
        marginLeft: wp("1%"),
        backgroundColor: "#64D2FF",
        borderRadius: hp("0.5%") / 2
    },
    progres_bar_style2:
    {
        width: hp("8.5%"),
        height: hp("0.5%"),
        marginLeft: wp("1%"),
        backgroundColor: "#3A6F84",
        borderRadius: hp("0.5%") / 2
    },
    linearstyle_border:
    {
        borderTopLeftRadius: hp("4%"),
        borderBottomLeftRadius: hp("4%"),
    },
    linearstyle_right_border:
    {
        borderTopRightRadius: hp("4%"),
        borderBottomRightRadius: hp("4%"),
    },
    linear_Annually:
    {
        width: wp('42%'),
        height: hp('5.8%'),
        justifyContent: "center",
        alignItems: "center",
        borderLeftWidth: hp('0.2%'),
        borderLeftColor: "#fff"
    },
    Annually:
    {
        height: hp('5.8%'),
        width: wp('42%'),
        //   backgroundColor:'yellow',
        alignItems: 'center',
        justifyContent: 'center',
        borderLeftWidth: hp('0.2%'),
        borderRightColor: "#ffffff"
    }
})